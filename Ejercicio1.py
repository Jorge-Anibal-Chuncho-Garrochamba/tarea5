# Ejercicio
# Autor =  "Jorge Anibal Chuncho Garrochamba"
# Email = "jorge.a.chuncho@unl.edu.ec"

# Realiazar una funcion que nos permita encontar numeros primos.

def encontrar_primos(inicial, final):

    primos = []

    if inicial < 2:
        inicial = 2

    for n in range(inicial, final+1):
        for i in range(2, n):
            if n % i == 0:
                break
        else:
            primos.append(n)

    return primos
a = int(input("Número Inicial: "))
b = int(input("Número Final: "))

print(encontrar_primos(a, b))